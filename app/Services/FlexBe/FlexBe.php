<?php

namespace App\Services\FlexBe;

class FlexBe
{
    // Статусы заявок
    public $leads_statuses = array(
        0 => 'Новая',
        1 => 'В работе',
        2 => 'Выполнена',
        10 => 'Отменена',
        11 => 'Удалена'
    );
    // Статусы оплат
    public $pays_statuses = array(
        0 => 'Ожидает оплаты',
        1 => 'В процессе оплаты',
        2 => 'Оплачен',
        3 => 'Ошибка оплаты'
    );

    // Массив с ошибками
    public $errors = array();

    // Параметры подключения к API (задаются при инициализации класса)
    public $api_url = false;
    public $api_key = false;

    /**
     *	Инициализация
     *
     *	@api_url string адрес для запросов к api
     *	@api_key string секретный ключ доступа к api
     */
    function __construct($api_url, $api_key) {
        $this->api_url = $api_url;
        $this->api_key = $api_key;
    }

    // Проверка доступности api
    public function checkConnection(){
        $response = $this->query('checkConnection', array());
        if ( $response['status'] == 1 ) {
            return true;
        }
        return false;
    }

    /**
     *	Вызов метода
     *
     *	@method string getLeads|changeLead
     *	@params array параметры метода
     */
    public function query($method, $params=array()){
        $query_params = $params;
        $query_params['method'] = $method;
        $query_params['api_key'] = $this->api_key;

        $curl = curl_init($this->api_url);
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($query_params)
        ));
        $response_json = curl_exec($curl);
        $response = json_decode($response_json, true);

        if (is_array($response['error'])) {
            $this->errors[] = $response['error'];
        }

        return $response;
    }


    /**
     *	Получение заявок
     *
     *	@filter array параметры метода
     */
    public function getLeads($filter=array()){
        return $this->query('getLeads', $filter);
    }


    /**
     *	Изменение заявки
     *
     *	@id int идентификатор заявки
     *	@fields array изменяемые поля
     */
    public function changeLead($id, $fields){
        $fields['id'] = $id;
        return $this->query('changeLead', $fields);
    }
}
