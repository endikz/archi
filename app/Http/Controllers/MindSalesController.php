<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MindSalesController extends Controller
{
    private $objectAssignmentList = [
        "Квартира" => 1,
        "Дом, коттедж" => 2,
        "Нежилое помещение (нп,вп)" => 3,
        "другое" => 4,
    ];

    private $whatNeedsToLegalizeList = [
        "Перепланировка" => 5,
        "Переоборудование" => 6,
        "Присоединение балкона, лоджии с комнатой/кухней " => 7,
        "Реконструкция" => 8,
        "Объединение" => 9,
        "Разделение" => 10,
        "Из нежилого в квартиру" => 11,
        "Захват кондоминиума" => 12,
        "Объединение+захват кондоминиума" => 13,
        "Новое строительство" => 14,
        "Новое со сносом старого" => 15,
        "Снос старого (если снесен)" => 16,
        "Снос старого (если не снесен)" => 17,
        "Возведение 2 этажа в нежилом помещении" => 18,
        "Перемер" => 19,
        "Технический проект" => 20,
        "Акт(самост)" => 21,
        "Технический проект+акт" => 22,
        "Акт(подрядный)" => 23,
        "Эскизный проект" => 24,
        "Форэскиз" => 25,
    ];

    private $loadBearingStructuresAffectedList = [
        "Да" => 26,
        "Нет" => 27
    ];
    private $realEstatePledgedList = [
        "Да" => 28,
        "Нет" => 29
    ];

    private $typeConstructionList = [
        "Новое строительство" => 30,
        "Реконструкция" => 31,
        "Новый со сносом старого" => 32,
        "Другое" => 33
    ];

    private $services = [
        "Перепланировка" => 54,
        "Переоборудование" => 55,
        "Присоединение балкона, лоджии с комнатой/кухней " => 56,
        "Реконструкция" => 57,
        "Объединение" => 58,
        "Разделение" => 59,
        "Из нежилого в квартиру" => 60,
        "Захват кондоминиума" => 61,
        "Объединение+захват кондоминиума" => 62,
        "Новое строительство" => 63,
        "Новое со сносом старого" => 64,
        "Снос старого (если снесен)" => 65,
        "Снос старого (если не снесен)" => 66,
        "Возведение 2 этажа в нежилом помещении" => 67,
        "Перемер" => 68,
        "Технический проект" => 69,
        "Акт(самост)" => 70,
        "Технический проект+акт" => 71,
        "Акт(подрядный)" => 72,
        "Эскизный проект" => 73,
        "Форэскиз" => 74,
    ];

    private $formData;

    public function postData(Request $request)
    {
        if (in_array($request->input('event'), array('lead', 'pay'))) {
            $lead = $request->input('data');
            $this->formData = $lead['form_data'];
            $clientData = [];
            $dealFields = [];

            foreach ($this->formData as $k => $item) {
                Log::info($item);
                switch ($item['name']) {
                    case "Name":
                        $clientData['name'] = $item['value'];
                        break;
                    case "Phone":
                        $clientData['phones'] = [$item['value']];
                        break;
                    case "Назначение":
                        $dealFields[] = [
                            "id" => 31,
                            "value" => $item['value'],
                        ];
                        break;
                    case "Этажность":
                        $dealFields[] = [
                            "id" => 43,
                            "value" => $item['value'],
                        ];
                        break;
                    case "Назначения объекта":
                        $dealFields[] = [
                            "id" => 42,
                            "value" => $this->getObjectAssignment($item['value']),
                        ];
                        break;
                    case "Площадь":
                    case "Площадь дома или коттеджа":
                    case "Площадь квартиры м2":
                        $dealFields[] = [
                            "id" => 32,
                            "value" => $item['value'],
                        ];
                        break;
                    case "Что нужно узаконить?":
                        $dealFields[] = [
                            "id" => 37,
                            "value" => $this->getWhatsNeedToLegalize($item['value']),
                        ];
                        break;
                    case "Что вы сделали?":
                        $dealFields[] = [
                            "id" => 38,
                            "value" => $item['value']
                        ];
                        break;
                    case "Затрагиваются ли несущие конструкции ?":
                        $dealFields[] = [
                            "id" => 39,
                            "value" => $this->getLoadBearingStructuresAffected($item['value']),
                        ];
                        break;
                    case "Отметьте имеющиеся документы":
                        $dealFields[] = [
                            "id" => 35,
                            "value" => $item['value']
                        ];
                        break;
                    case "Недвижимость в залоге?":
                        $dealFields[] = [
                            "id" => 40,
                            "value" => $this->getRealEstatePledged($item['value'])
                        ];
                        break;
                    case "Откуда о нас узнали ?":
                        $dealFields[] = [
                            "id" => 36,
                            "value" => $item['value']
                        ];
                        break;

                    case "Комментарии":
                        $dealFields[] = [
                            "id" => 26,
                            "value" => $item['value']
                        ];
                        break;

                    case "Где находится участок ?":
                        $dealFields[] = [
                            "id" => 27,
                            "value" => $item['value']
                        ];
                        break;
                    case "Какой поселок ?":
                        $dealFields[] = [
                            "id" => 28,
                            "value" => $item['value']
                        ];
                        break;
                    case "Какой район в городе ?":
                        $dealFields[] = [
                            "id" => 41,
                            "value" => $item['value']
                        ];
                        break;

                    case "Вид строительство":
                        $dealFields[] = [
                            "id" => 29,
                            "value" => $this->getTypeContraction($item['value'])
                        ];
                        break;

                    case "Наличие застройки на участке":
                        $dealFields[] = [
                            "id" => 30,
                            "value" => $this->getTypeContraction($item['value'])
                        ];
                        break;

                    case "Стена":
                        $dealFields[] = [
                            "id" => 33,
                            "value" => $item['value']
                        ];
                        break;
                    case "Наружная отделка":
                        $dealFields[] = [
                            "id" => 34,
                            "value" => $item['value']
                        ];
                        break;
                    case "Выберите услугу:":
                        $dealFields[] = [
                            "id" => 44,
                            "value" => $this->getService($item['value'])
                        ];
                        break;

                }
            }

            Log::info('clientData', $clientData);
            Log::info('fields', $dealFields);

            $this->deals($clientData, $dealFields);

            return true;
        }
        return response()->json('false');
    }


    private function getObjectAssignment($value)
    {
        return $this->objectAssignmentList[$value] ?? 4;
    }

    private function getLoadBearingStructuresAffected($value)
    {
        return $this->loadBearingStructuresAffectedList[$value] ?? null;
    }

    private function getWhatsNeedToLegalize($value)
    {
        return $this->whatNeedsToLegalizeList[$value] ?? null;
    }

    private function getRealEstatePledged($value)
    {
        return $this->realEstatePledgedList[$value] ?? null;
    }

    private function getTypeContraction($value){
        return $this->typeConstructionList[$value] ?? null;
    }

    private function getService($value)
    {
        return $this->services[$value] ?? null;
    }

    public function deals($clientData, $dealFields)
    {
        $data = [
            'data' => [
                [
                    "clientSourceId" => 11,
                     "clientManagerId" => null,
                    "phones" => $clientData['phones'] ?? [],
                    "clientFields" => [
                        [
                            "id" => 1,
                            "value" => $clientData['name'] ?? 'Без имени',
                        ]
                    ],
                    "createDealIfExistsClient" => true,
                    "deals" => [
                        [
                            "dealFunnelStepId" => 1,
                            "dealStatusId" => 1,
                            "dealFields" => $dealFields
                        ]
                    ]
                ]
            ]
        ];

        return $this->request('addclientsdeals', $data);
    }

    private function request($method, $data)
    {
        Log::info(json_encode($data));
        $curl = curl_init(); // Инициализируем запрос
        curl_setopt_array($curl, array(
            CURLOPT_URL => env('URL_MINDSALES') . $method, // Полный адрес метода
            CURLOPT_RETURNTRANSFER => true, // Возвращать ответ
            CURLOPT_POST => true, // Метод POST
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Connection: Keep-Alive'
            ]
        ));

        $response = curl_exec($curl); // Выполняем запрос

        $response = json_decode($response, true); // Декодируем из JSON в массив
        curl_close($curl); // Закрываем соединение
        return $response; // Возвращаем ответ
    }
}
